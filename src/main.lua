import "libraries/noble/Noble"

import "scenes/mandelbrot"

Noble.Settings.setup({
    Difficulty = "Medium"
})

Noble.showFPS = true

Noble.new(MandelbrotScene)
