const std = @import("std");
const builtin = @import("builtin");
const pdapi = @import("playdate.zig");
const pdz = @import("playdate_zig.zig");

// Logger
pub const std_options = .{
    .log_level = .debug,
    .logFn = pdz.logFn,
};

const PixelBuffer = std.packed_int_array.PackedIntSliceEndian(u1, .big);

const c32 = std.math.Complex(f32);

inline fn mandelbrot_step(z: c32, c: c32) c32 {
    // @setFloatMode(.Optimized);
    return z.mul(z).add(c);
}

inline fn abs2(z: c32) f32 {
    // @setFloatMode(.Optimized);
    return z.im * z.im + z.re * z.re;
}

fn mandelbrot_test(z_start: c32, c: c32, steps: usize) usize {
    // @setFloatMode(.Strict);

    // var z = z_start;
    // for (1..steps) |step| {
    //     if (abs2(z) > 4.0) {
    //         return step;
    //     }
    //     z = mandelbrot_step(z, c);
    // }

    var x = z_start.re;
    var y = z_start.im;
    for (1..steps) |step| {
        if (x * x + y * y >= 4.0) {
            return step;
        }
        const new_x = x * x - y * y + c.re;
        const new_y = 2.0 * x * y + c.im;
        x = new_x;
        y = new_y;
    }

    return 0;
}

fn registerMandelbrotFuncs(pd: *pdapi.PlaydateAPI) !void {
    try pdz.class.registerGlue(
        pd,
        "fractals",
        struct {
            pub fn drawMandelbrot(bitmap: *pdapi.LCDBitmap, c_a: f32, c_b: f32, zoom_level: f32) i32 {
                // @setFloatMode(.Optimized);

                // std.log.debug("Starting drawing.", .{});

                const c = c32.init(c_a, c_b);
                // std.log.debug("Drawing that mandelbrot set at z:{d}", .{zoom_level});

                var width: i32 = 0;
                var height: i32 = 0;
                var rowbytes: i32 = 0;
                var mask: [*c]u8 = null;
                var data: [*c]u8 = null;
                pdz.pd.graphics.getBitmapData(bitmap, &width, &height, &rowbytes, &mask, &data);

                const height_usize: usize = @intCast(height);
                const width_usize: usize = @intCast(width);
                const rowbytes_usize: usize = @intCast(rowbytes);

                const data_slice: []u8 = data[0 .. rowbytes_usize * height_usize];
                var pixelbuffer = PixelBuffer.init(data_slice, 8 * rowbytes_usize * height_usize);
                // var alphabuffer = PixelBuffer.init(mask[0 .. rowbytes_usize * height_usize], 8 * rowbytes_usize * height_usize);

                // std.log.debug("That buffer: {}", .{pixelbuffer});

                const center_x: isize = @intCast(width_usize / 2);
                const center_y: isize = @intCast(height_usize / 2);

                if (width_usize % 8 != 0) {
                    unreachable;
                }

                for (0..height_usize) |y| {
                    for (0..width_usize) |x| {
                        // for (0..width_usize / 8) |x_byte| {
                        //     var byte: u8 = 0;

                        //     inline for (0..8) |bit| {
                        //         const x_usize = x_byte * 8 + bit;

                        const x_f32: f32 = @floatFromInt(@as(isize, @intCast(x)) - center_x);
                        const y_f32: f32 = @floatFromInt(@as(isize, @intCast(y)) - center_y);

                        const z = c32.init(x_f32 / zoom_level, y_f32 / zoom_level).add(c);

                        //         byte |= @as(u8, @intCast(mandelbrot_test(c32.init(0, 0), z, 30) & 1)) << (7 - bit);
                        //     }
                        //     data_slice[y * rowbytes_usize + x_byte] = byte;

                        const index = x + y * rowbytes_usize * 8;

                        // if (mandelbrot_test(c32.init(0, 0), z, 30) == 0) {
                        //     pixelbuffer.set(index, 1);
                        // } else {
                        //     pixelbuffer.set(index, 0);
                        // }
                        pixelbuffer.set(
                            index,
                            @intCast(mandelbrot_test(c32.init(0, 0), z, 30) & 1),
                        );
                    }
                }

                // std.log.debug("Finished drawing.", .{});
                return 0;
            }
        },
        true,
    );

    return;
}

pub export fn eventHandler(playdate: *pdapi.PlaydateAPI, event: pdapi.PDSystemEvent, arg: u32) callconv(.C) c_int {
    _ = arg;
    switch (event) {
        .EventInit => {
            pdz.init(playdate);
        },
        .EventInitLua => {
            std.log.info("Lua Init", .{});

            if (pdz.isSimulator) {
                std.log.info("Is simulator.", .{});
            } else {
                std.log.info("Is Playdate.", .{});
            }

            registerMandelbrotFuncs(playdate) catch return 0;
        },
        else => {},
    }
    return 0;
}
