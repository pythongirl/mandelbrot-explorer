const std = @import("std");
const pdapi = @import("playdate.zig");

pub var pd: *pdapi.PlaydateAPI = undefined;
pub var pda: std.mem.Allocator = std.mem.Allocator{
    .ptr = undefined,
    .vtable = PDAllocator.VTable,
};

pub fn init(playdate: *pdapi.PlaydateAPI) void {
    pd = playdate;
    pda.ptr = playdate;
}

// Logger
// From David's code
// https://github.com/DavidMedin/playdate_roguelike/blob/master/src/main.zig#L83-L94
pub fn logFn(comptime level: std.log.Level, comptime scope: @TypeOf(.EnumLiteral), comptime format: []const u8, args: anytype) void {
    _ = scope;
    const prefix = "[" ++ comptime level.asText() ++ "] ";
    const fmtd_string: [:0]u8 = std.fmt.allocPrintZ(pda, prefix ++ format, args) catch unreachable;
    nosuspend pd.system.logToConsole(@ptrCast(fmtd_string));
    pda.free(fmtd_string);
}

// Allocator
// This allocator is mostly from David's code
// https://github.com/DavidMedin/playdate_roguelike/blob/master/src/main.zig#L100-L136
pub const PDAllocator = struct {
    const VTable = &std.mem.Allocator.VTable{
        .alloc = alloc,
        .resize = resize,
        .free = free,
    };

    // fn new(playdate: *pdapi.PlaydateAPI) std.mem.Allocator {
    //     return std.mem.Allocator{ .ptr = playdate, .vtable =  };
    // }

    fn alloc(ctx: *anyopaque, len: usize, _: u8, _: usize) ?[*]u8 {
        const playdate: *pdapi.PlaydateAPI = @ptrCast(@alignCast(ctx));

        // TODO: Don't just throw away the ptr_align thing.
        return @ptrCast(playdate.system.realloc(null, len));
    }

    fn resize(_: *anyopaque, _: []u8, _: u8, _: usize, _: usize) bool {
        return false; // Playdate doesn't have a function to *attempt* to resize without moving. It will just allocate it.
        // So, lets hope that every user of .resize will have a fallback 'free' and 'alloc'.
    }

    fn free(ctx: *anyopaque, buf: []u8, _: u8, _: usize) void {
        const playdate: *pdapi.PlaydateAPI = @ptrCast(@alignCast(ctx));
        _ = playdate.system.realloc(buf.ptr, 0);
    }
};

pub const isSimulator = @import("config").isSimulator;

pub const class = struct {
    pub const RegisterError = error{Registration};

    inline fn wrap_function_args(comptime Args: type, current_args: anytype) Args {
        const full_arg_count = @typeInfo(Args).Struct.fields.len;
        const current_arg_count = @typeInfo(@TypeOf(current_args)).Struct.fields.len;

        // If i dont have this check the zig compiler crashes silently. :/
        if (full_arg_count == 0) {
            return .{};
        }

        if (full_arg_count == current_arg_count) {
            return current_args;
        } else {
            const next_arg = lua.getArg(@typeInfo(Args).Struct.fields[current_arg_count].type, current_arg_count + 1);
            return wrap_function_args(Args, current_args ++ .{next_arg});
        }
    }

    pub fn registerGlue(playdate: *pdapi.PlaydateAPI, name: [*:0]const u8, comptime reg: type, is_static: bool) RegisterError!void {
        const null_reg = pdapi.LuaReg{
            .name = null,
            .func = null,
        };

        const reg_decls = @typeInfo(reg).Struct.decls;
        comptime var functions: [reg_decls.len:null_reg]pdapi.LuaReg = undefined;
        inline for (reg_decls, 0..) |d, i| {
            const func = @field(reg, d.name);
            functions[i] = pdapi.LuaReg{
                .name = d.name,
                .func = switch (@typeInfo(@TypeOf(func)).Fn.calling_convention) {
                    .C => @field(reg, d.name),
                    else => (struct {
                        pub fn wrapper(_: ?*pdapi.LuaState) callconv(.C) c_int {
                            return @call(
                                .auto,
                                func,
                                wrap_function_args(std.meta.ArgsTuple(@TypeOf(func)), .{}),
                            );
                            // return func();
                        }
                    }).wrapper,
                },
            };
        }

        var errMsg: [*c]const u8 = null;
        if (0 == playdate.lua.registerClass(name, &functions, null, if (is_static) 1 else 0, &errMsg)) {
            playdate.system.logToConsole("%s registerClass failed, %s", name, errMsg);
            return error.Registration;
        }
    }

    pub inline fn isClass(comptime T: type) bool {
        return @hasDecl(T, "__pdz_class");
    }
};

pub const lua = struct {
    pub fn getArgBool(pos: i32) bool {
        return pd.lua.getArgBool(pos) != 0;
    }

    pub fn getArgBytes(pos: i32) []const u8 {
        var length: usize = 0;
        const string_ptr = pd.lua.getArgBytes(pos, &length);
        return string_ptr[0..length];
    }

    pub fn getArgObject(comptime T: type, pos: i32) *T {
        return @ptrCast(@alignCast(pd.*.lua.getArgObject(pos, T.__pdz_class, null)));
    }

    pub fn getArg(comptime T: type, pos: i32) T {
        if (T == []const u8) {
            return getArgBytes(pos);
        } else {
            switch (@typeInfo(T)) {
                .Optional => |optional| {
                    if (pd.lua.argIsNil(pos) != 0) {
                        return null;
                    } else {
                        return getArg(optional.child, pos);
                    }
                },
                .Pointer => |ptr_info| {
                    if (ptr_info.child == pdapi.LCDSprite) {
                        return pd.lua.getSprite(pos).?;
                    } else if (ptr_info.child == pdapi.LCDBitmap) {
                        return pd.lua.getBitmap(pos).?;
                    } else if (class.isClass(ptr_info.child)) {
                        return getArgObject(ptr_info.child, pos);
                    } else {
                        @compileError("Unsupported pointer type");
                    }
                },
                .Int => if (T == i32) {
                    return pd.lua.getArgInt(pos);
                } else {
                    @compileError("i32 is the only supported integer type.");
                },
                .Float => if (T == f32) {
                    return pd.lua.getArgFloat(pos);
                } else {
                    @compileError("f32 is the only supported float type.");
                },
                .Bool => {
                    return pd.lua.getArgBool(pos);
                },
                else => {
                    @compileError("Unsupported arg type.");
                },
            }
        }
    }

    pub fn pushObject(comptime T: type, obj: *T, nValues: i32) ?*pdapi.LuaUDObject {
        return pd.lua.pushObject(obj, T.__pdz_class, nValues);
    }

    pub fn pushReturn(values: anytype) i32 {
        inline for (values) |value| {
            const value_type = @TypeOf(value);

            switch (@typeInfo(value_type)) {
                .Float => if (value_type == f32) {
                    pd.lua.pushFloat(value);
                } else {
                    @compileError("f32 is the only supported float type.");
                },
                .Int => if (value_type == i32) {
                    pd.lua.pushInt(value);
                } else {
                    @compileError("i32 is the only supported integer type.");
                },
                .Bool => {
                    pd.lua.pushBool(if (value) 1 else 0);
                },
                .Pointer => |ptr| {
                    if (class.isClass(ptr.child)) {
                        _ = pushObject(ptr.child, value, 0);
                    } else if (ptr.child == pdapi.LCDSprite) {
                        pd.lua.pushSprite(value);
                    } else if (ptr.child == pdapi.LCDBitmap) {
                        pd.lua.pushBitmap(value);
                    } else {
                        @compileError("Only pdz class pointers are supported.");
                    }
                },
                else => if (type == []const u8) {
                    pd.lua.pushBytes(value.ptr, value.len);
                } else {
                    @compileError("Unsupported return type.");
                },
            }
        }
        return @typeInfo(@TypeOf(values)).Struct.fields.len;
    }
};
