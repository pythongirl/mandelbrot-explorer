local gfx <const> = playdate.graphics

MandelbrotScene = {}
class("MandelbrotScene").extends(NobleScene)
local scene = MandelbrotScene

function scene:init()
    scene.super.init(self)

    self.image = gfx.image.new("images/playdate_image")
    assert(self.image, "creating image failed")

    self.pause_base = gfx.image.new("images/Pause")
    assert(self.pause_base, "failed to create pause base image")

    self.buffer = gfx.image.new(400, 240, gfx.kColorWhite)

    self.zoom_level = 80.0
    self.x = 0
    self.y = 0

    self.inputHandler = {
        upButtonHold = function()
            self.y = self.y - 1 / self.zoom_level
        end,
        downButtonHold = function()
            self.y = self.y + 1 / self.zoom_level
        end,
        leftButtonHold = function()
            self.x = self.x - 1 / self.zoom_level
        end,
        rightButtonHold = function()
            self.x = self.x + 1 / self.zoom_level
        end,
        AButtonDown = function()
            playdate.resetElapsedTime()
            fractals.drawMandelbrot(self.buffer, self.x, self.y, self.zoom_level)
            print(string.format("Took: %g ms", playdate.getElapsedTime() * 1000))
        end,
        cranked = function(change)
            self.zoom_level = self.zoom_level * (1.0 + change * 0.01)
        end,
    }
end

function scene:drawBackground()
    scene.super.drawBackground(self)
    -- self.gfx.drawBackground
    self.buffer:draw(0, 0)

    -- self.image:draw(200, 120)
    -- Noble.Text.draw("Hello from zig & lua!!!!", 2, 16)
end

function scene:update()
    scene.super.init(self)
end

function scene:pause()
    scene.super.pause(self)
    --[Your code here]--

    local pause_screen = self.pause_base:copy()

    gfx.pushContext(pause_screen)
    Noble.Text.draw(
        string.format("Paused\nX: %g\nY: %g\nZ: %g", self.x, self.y, self.zoom_level),
        12, 12
    )
    gfx.popContext()

    playdate.setMenuImage(pause_screen)
end
